﻿namespace MarkDocCommon
{
  public static class Constants
  {
    // Parameter switches
    public const string SW_HELP = "help";
    public const string SW_GENENERATE_DIRS = "gendirs";
    public const string SW_IGNORE_XML = "noxml";

    // Parameter arguments
    public const string ARG_ASSEMBLIES = "asm";
    public const string ARG_XML = "xml";
    public const string ARG_OUTPUT = "out";

    // File extensions
    public const string EXT_MARKDOWN = "md";
    public const string EXT_XML = "xml";

    // Tags
    public const string TAG_SUMMARY = "summary";
    public const string TAG_RETURNS = "returns";
    public const string TAG_PARAM = "param";
    public const string TAG_PARAMREF = "paramref";
    public const string TAG_SEE = "see";
    public const string TAG_SEEALSO = "seealso";
    public const string TAG_TYPEPARAM = "typeparam";
    public const string TAG_TYPEPARAMREF = "typeparamref";
    public const string TAG_LIST = "list";
    public const string TAG_LISTHEADER = "listheader";
    public const string TAG_TERM = "term";
    public const string TAG_DESCRIPTION = "description";
    public const string TAG_ITEM = "item";
    public const string TAG_REMARKS = "remarks";
    public const string TAG_EXCEPTION = "exception";
    public const string TAG_INHERITDOC = "inheritdoc";
    public const string TAG_PARA = "para";
    public const string TAG_CODE = "c";
    public const string TAG_CODEBLOCK = "code";
    public const string TAG_EXAMPLE = "example";

    // Attributes
    public const string ATTR_REFERENCE = "cref";
    public const string ATTR_NAME = "name";
    public const string ATTR_TYPE = "type";

    public const string ATTR_VAL_BULLET = "bullet";
    public const string ATTR_VAL_NUMBER = "number";
    public const string ATTR_VAL_TABLE = "table";

    // Page sections
    public const string SECTION_CLASSES = "Classes";
    public const string SECTION_ENUMS = "Enums";
    public const string SECTION_PUBLIC_METHODS = "Public Member Functions";
    public const string SECTION_STATIC_PUBLIC_METHODS = "Static Public Member Functions";
    public const string SECTION_PROTECTED_METHODS = "Protected Member Functions";
    public const string SECTION_STATIC_PROTECTED_METHODS = "Static Protected Member Functions";
    public const string SECTION_PROPERTIES = "Properties";
    public const string SECTION_METHOD_DOCUMENTATION = "Member Function Documentation";
    public const string SECTION_LONG_DESCRIPTION = "Detailed Description";
    public const string SECTION_TEMPLATE_PARAMS = "Template Parameters";
    public const string SECTION_TABLE_OF_CONTENTS = "Table of contents";
    public const string SECTION_INHERITANCE = "Inheritance";
    public const string SECTION_CLASS_NAME = "Class name";
    public const string SECTION_ENUM_NAME = "Enum name";
    public const string SECTION_CONSTRUCTORS = "Constructors";

    // Labels
    public const string LABEL_NAME = "Name";
    public const string LABEL_VALUE = "Value";
    public const string LABEL_RETURNS = "Returns";
    public const string LABEL_EXCEPTIONS = "Exceptions";
    public const string LABEL_DESCRIPTION = "Descritpion";
    public const string LABEL_TYPE = "Type";
    public const string LABEL_CONSTRAINT = "Constraints";
    public const string LABEL_RETURN_TYPE = "Returns type";
    public const string LABEL_PARAMETERS = "Parameters";
    public const string LABEL_REMARKS = "Remarks";
    public const string LABEL_EXAMPLE = "Example";
  }
}