﻿namespace MarkDocCommon
{
  /// <summary>
  /// Pair class
  /// </summary>
  /// <typeparam name="TA">Type A</typeparam>
  /// <typeparam name="TB">Type B</typeparam>
  public struct Pair<TA, TB>
  {
    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="itemA">First item</param>
    /// <param name="itemB">Second item</param>
    public Pair(TA itemA, TB itemB) : this()
    {
      ItemA = itemA;
      ItemB = itemB;
    }

    /// <summary>
    /// Pair element
    /// </summary>
    public TA ItemA { get; set; }

    /// <summary>
    /// Pair element
    /// </summary>
    public TB ItemB { get; set; }
  }
}