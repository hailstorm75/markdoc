﻿using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MarkDocCommon
{
  /// <summary>
  /// Collection of text formatting methods
  /// </summary>
  public static class TextFormatter
  {
    /// <summary>
    /// Applies italic font style
    /// </summary>
    /// <param name="text">Text to format</param>
    /// <returns>Formatted text</returns>
    public static string ToItalic(this string text)
      => text.StartsWith("*") || text.EndsWith("*") ? $"_{text}_" : $"*{text}*";

    /// <summary>
    /// Applies bold font style
    /// </summary>
    /// <param name="text">Text to format</param>
    /// <returns>Formatted text</returns>
    public static string ToBold(this string text)
      => text.StartsWith("*") || text.EndsWith("*") ? $"__{text}__" : $"**{text}**";

    /// <summary>
    /// Applies scratched font style
    /// </summary>
    /// <param name="text">Text to format</param>
    /// <returns>Formatted text</returns>
    public static string ToScratched(this string text)
      => $"~~{text}~~";

    /// <summary>
    /// Turns <paramref name="text"/> to inline code
    /// </summary>
    /// <param name="text">Text to format</param>
    /// <returns>Formatted text</returns>
    public static string ToCode(this string text)
      => $"`{text.Trim()}`";

    /// <summary>
    /// Turns <paramref name="text"/> to code block
    /// </summary>
    /// <param name="text">Text to format</param>
    /// <returns>Formatted text</returns>
    public static string ToCodeBlock(this string text)
      => $"```csharp\n{text}\n```";

    /// <summary>
    /// Turns <paramref name="text"/> into link
    /// </summary>
    /// <param name="text">Text to format</param>
    /// <param name="link">Link to</param>
    /// <returns>Formatted text</returns>
    public static string ToLink(this string text, string link)
      => $"[{text}]({link})";

    /// <summary>
    /// Puts <paramref name="text"/> into block quote
    /// </summary>
    /// <param name="text">Text to format</param>
    /// <returns>Formatted text</returns>
    public static string ToBlockQuote(this string text)
      => $"> {text}";

    /// <summary>
    ///
    /// </summary>
    /// <param name="strings"></param>
    /// <returns></returns>
    public static string ToMultiline(params string[] strings)
      => string.Join("<br>", strings.Where(x => !string.IsNullOrEmpty(x)));

    public static string EscapeSpecial(this string text)
    {
      var prevMatchIndex = 0;
      var newStr = new StringBuilder();
      var matches = new Regex(@"(<|>)").Matches(text);

      foreach (Match match in matches)
      {
        newStr.Append(text.Substring(prevMatchIndex, match.Index - prevMatchIndex)).Append($@"\{match.Value}");
        prevMatchIndex = match.Index + 1;
      }

      return matches.Count > 0 ? newStr.ToString() : text;
    }

    public static string RemoveSpecial(this string text)
      => new Regex(@"(<|>|\.)").Replace(text, string.Empty);
  }
}