﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MarkDoc.Console
{
  /// <summary>
  ///
  /// </summary>
  public sealed class ArgumentParser
  {
    #region Fields

    private readonly string m_paramInitial;
    private readonly Dictionary<string, bool> m_switches;
    private Dictionary<string, string> m_arguments;
    private readonly bool m_caseSensetive;

    #endregion

    private enum ParamType
    {
      Switch,
      Argument,
      Error
    }

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="input"></param>
    /// <param name="paramInitial"></param>
    /// <param name="caseSensetive"></param>
    /// <param name="expectedSwitches"></param>
    /// <param name="expectedArguments"></param>
    public ArgumentParser(IReadOnlyList<string> input, string paramInitial = "/", bool caseSensetive = true,
      IReadOnlyCollection<string> expectedSwitches = null, IReadOnlyCollection<string> expectedArguments = null)
    {
      if (input.Count == 0)
        throw new ArgumentException("Parameter list cannot be empty");
      if (!input[0].StartsWith(paramInitial))
        throw new ArgumentException("Invalid parameter format");
      if (expectedSwitches == null
          && expectedArguments == null)
        throw new ArgumentException($"Both {nameof(expectedSwitches)} and {nameof(expectedArguments)} cannot be null");
      if (expectedSwitches?.Count == 0
          && expectedArguments?.Count == 0)
        throw new ArgumentException($"Both {nameof(expectedSwitches)} and {nameof(expectedArguments)} cannot be empty");
      if (expectedSwitches?.Intersect(expectedArguments ?? new List<string>()).Any() ?? false)
        throw new ArgumentException("A paramater cannot be both a switch and an argument");

      m_paramInitial = paramInitial;
      m_caseSensetive = caseSensetive;

      if (m_caseSensetive)
      {
        m_switches = expectedSwitches?.Select(x => x).ToDictionary(x => x, x => false);
        m_arguments = expectedArguments?.Select(x => x).ToDictionary(x => x, x => default(string));
      }
      else
      {
        m_switches = expectedSwitches?.Select(x => x.ToLowerInvariant()).ToDictionary(x => x, x => false);
        m_arguments = expectedArguments?.Select(x => x.ToLowerInvariant()).ToDictionary(x => x, x => default(string));
      }

      Parse(input);
    }

    #region Methods

    /// <summary>
    ///
    /// </summary>
    /// <param name="input">List of parameters</param>
    /// <exception cref="ArgumentException"></exception>
    private void Parse(IReadOnlyList<string> input)
    {
      for (var i = 0; i < input.Count; i++)
      {
        if (!input[i].StartsWith(m_paramInitial))
          throw new ArgumentException($"Error in {nameof(Parse)} method. Invalid parameter.");

        var noInitial = m_caseSensetive
          ? input[1].Substring(1)
          : input[i].Substring(1).ToLowerInvariant();

        switch (GetParamType(noInitial))
        {
          case ParamType.Switch:
            m_switches[noInitial] = true;
            break;
          case ParamType.Argument:
            m_arguments[noInitial] = i + 1 < input.Count
              ? input[++i]
              : string.Empty;
            break;
          case ParamType.Error:
          default:
            throw new ArgumentException($"Error in {nameof(Parse)} method. Invalid parameter.");
        }
      }
    }

    private ParamType GetParamType(string input)
    {
      if (m_switches.ContainsKey(input))
        return ParamType.Switch;
      if (m_arguments.ContainsKey(input))
        return ParamType.Argument;

      return ParamType.Error;
    }

    /// <summary>
    /// Retrieves switch state
    /// </summary>
    /// <param name="switchName">Switch to check</param>
    /// <returns>Switch state</returns>
    public bool SwitchState(string switchName) => m_switches.ContainsKey(switchName) && m_switches[switchName];

    /// <summary>
    /// Retrieves argument value
    /// </summary>
    /// <param name="argumentName">Argument to get</param>
    /// <returns>Argument value</returns>
    public string ArgumentValue(string argumentName) =>
      m_arguments.ContainsKey(argumentName) ? m_arguments[argumentName] : default;

    #endregion
  }
}