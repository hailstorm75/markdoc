﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using MarkDocCommon;
using MarkDocGenerator;
using MarkDocElements;
using static System.Console;

namespace MarkDoc.Console
{
  internal static class Program
  {
    #region Readonly Fields

    private const string PROG_NAME = "MarkDoc";
    private const string PARAM_INITIAL = "/";

    private const string ERR_NO_FILE = "File does not exist";

    private static readonly string[] m_switches =
    {
      Constants.SW_HELP,
      Constants.SW_GENENERATE_DIRS,
      Constants.SW_IGNORE_XML
    };

    private static readonly string[] m_arguments =
    {
      Constants.ARG_ASSEMBLIES,
      Constants.ARG_XML,
      Constants.ARG_OUTPUT
    };

    private static readonly string m_manual = $@"-- {PROG_NAME} manual --

Switches:
    {PARAM_INITIAL}{Constants.SW_HELP} - shows this manual. Every other parameter will be ignored.
    {PARAM_INITIAL}{
        Constants.SW_GENENERATE_DIRS
      } - generates output directory paths if they do not exist. Otherwise an error will occur if a directory in given path isn't present.
    {PARAM_INITIAL}{
        Constants.SW_IGNORE_XML
      } - ignores xml documentation file and generates from executable/library file only.

Arguments:
    {PARAM_INITIAL}{Constants.ARG_ASSEMBLIES} - path to assembly file. Ex: {PARAM_INITIAL}{
        Constants.ARG_ASSEMBLIES
      } user/binary.dll
                                      You can also provide multiple paths at once. Ex: {PARAM_INITIAL}{
        Constants.ARG_ASSEMBLIES
      } {"\"user/binaryA.dll, user/binaryB.dll\""}
    {PARAM_INITIAL}{
        Constants.ARG_XML
      } - path to folder with xml documentation for provided executable/library file(s). Assumes documentation is in the same directory if argument not used.
    {PARAM_INITIAL}{Constants.ARG_OUTPUT} - path to output directory.";

    #endregion

    #region Fields

    private static string[] m_pathsToAssemblies;

    #endregion

    private static void Main(string[] args)
    {
      Run(args);

      WriteLine("Done");

      ReadKey();
    }

    private static void Run(string[] args)
    {
      ArgumentParser argsParsed;
      try
      {
        argsParsed = ProcessArguments(args);
      }
      catch (Exception e)
      {
        WriteLine($"An error has occured.\nError: {e}\n\nUse the '{Constants.SW_HELP}' switch for the manual.");
        return;
      }

      if (argsParsed.SwitchState(Constants.SW_HELP))
      {
        WriteLine(m_manual);
        return;
      }

      GenerateMarkdown(argsParsed);
    }

    private static void GenerateMarkdown(ArgumentParser argParser)
    {
      // TODO Splitting like this won't do
      m_pathsToAssemblies = argParser.ArgumentValue(Constants.ARG_ASSEMBLIES)?.Split(' ');
      if (m_pathsToAssemblies == null)
        throw new ArgumentException($"{Constants.ARG_ASSEMBLIES} contains invalid path(s).");

      var pairs = BindAssembliesToDocs(argParser.SwitchState(Constants.SW_IGNORE_XML)
        ? null
        : argParser.ArgumentValue(Constants.ARG_XML) ?? string.Empty);

      var assemblyPages = pairs.Select(pair =>
        new Page(pair.ItemA.FullName.Substring(0, pair.ItemA.FullName.IndexOf(",", StringComparison.InvariantCulture)))
          .SetContent(Generator.Generate(pair.ItemA, pair.ItemB)));
      var mainPage = new Page("Home");
      mainPage.SetContent(assemblyPages);

      MarkDocExporter.Exporter.Export(mainPage, argParser.ArgumentValue(Constants.ARG_OUTPUT), new System.Threading.CancellationToken());
    }

    private static ArgumentParser ProcessArguments(IReadOnlyList<string> args)
    {
      var argParser = new ArgumentParser(args, PARAM_INITIAL, false, m_switches, m_arguments);

      if (argParser.ArgumentValue(Constants.ARG_ASSEMBLIES)?.Trim() == string.Empty
          || argParser.ArgumentValue(Constants.ARG_OUTPUT)?.Trim() == string.Empty)
        throw new Exception("Invalid parameter format.");

      return argParser;
    }

    private static IEnumerable<Pair<Assembly, XmlDocument>> BindAssembliesToDocs(string xmlPath)
    {
      foreach (var pathToAssembly in m_pathsToAssemblies)
      {
        if (!File.Exists(pathToAssembly))
        {
          WriteLine(ERR_NO_FILE);
          continue;
        }

        XmlDocument documentation = null;

        if (xmlPath == string.Empty)
        {
          documentation = new XmlDocument();
          try
          {
            documentation.Load(Path.Combine(
              Path.GetDirectoryName(pathToAssembly) ?? throw new ArgumentException("Invalid path."),
              $"{Path.GetFileNameWithoutExtension(pathToAssembly) ?? throw new ArgumentException("Invalid path.")}.{Constants.EXT_XML}"));
          }
          catch (Exception e)
          {
            WriteLine(e);
            continue;
          }
        }
        else if (!string.IsNullOrEmpty(xmlPath))
        {
          if (!File.Exists(xmlPath))
          {
            WriteLine(ERR_NO_FILE);
            continue;
          }

          try
          {
            documentation = new XmlDocument();
            documentation.Load(Path.Combine(xmlPath,
              $"{Path.GetFileNameWithoutExtension(pathToAssembly) ?? throw new ArgumentException("Invalid path.")}.xml"));
          }
          catch (Exception e)
          {
            WriteLine(e);
            continue;
          }
        }

        yield return new Pair<Assembly, XmlDocument>(Assembly.LoadFile(pathToAssembly), documentation);
      }
    }
  }
}