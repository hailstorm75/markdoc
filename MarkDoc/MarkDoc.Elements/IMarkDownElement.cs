﻿namespace MarkDocElements
{
  /// <summary>
  /// Interface for mark down elements
  /// </summary>
  public interface IMarkDownElement
  {
    /// <summary>
    /// Element text heading
    /// </summary>
    string HeadingText { get; }

    /// <summary>
    /// Element heading level
    /// </summary>
    int HeadingLevel { get; }

    /// <summary>
    /// True if heading is defined
    /// </summary>
    bool HasHeading { get; }

    /// <summary>
    /// Converts given element to markdown
    /// </summary>
    /// <returns>Markdown</returns>
    string ToString();
  }
}