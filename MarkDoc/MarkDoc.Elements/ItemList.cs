﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarkDocElements
{
  /// <summary>
  /// Markdown element representing a list
  /// </summary>
  public sealed class ItemList
    : MarkDownElement
  {
    /// <summary>
    /// Possilbe list types
    /// </summary>
    public enum ListType
    {
      /// <summary>
      /// List items will be numbered
      /// </summary>
      Numbered,

      /// <summary>
      /// List items will be dotted
      /// </summary>
      Dotted
    }

    #region Fields

    private readonly ListType m_type;
    private readonly List<object> m_items;

    #endregion

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="type">List type</param>
    /// <param name="headingText">Element heading text</param>
    /// <param name="headingLevel">Element heading level</param>
    public ItemList(ListType type, string headingText = "", int headingLevel = 0)
      : base(headingText, headingLevel)
    {
      m_type = type;
      m_items = new List<object>();
    }

    #region Methods

    /// <summary>
    /// Add a new item to list
    /// </summary>
    /// <param name="text">Item to add</param>
    /// <returns></returns>
    public ItemList Add(string text)
    {
      m_items.Add(text);
      return this;
    }

    /// <summary>
    /// Add a new item to list
    /// </summary>
    /// <param name="list">Item to add</param>
    /// <returns></returns>
    public ItemList Add(ItemList list)
    {
      m_items.Add(list);
      return this;
    }

    /// <inheritdoc cref="MarkDownElement.ToString()" />
    public override string ToString()
      => ToString(1);

    private string ToString(int indent)
    {
      var result = new StringBuilder();
      if (HasHeading)
        result.Append(base.ToString());
      for (var i = 0; i < m_items.Count; i++)
      {
        var item = m_items[i];
        if (item is ItemList list)
          result.Append($"{list.ToString(indent + 1)}");
        else
        {
          result.Append($"{new string(' ', indent * 2 - 1)}");
          switch (m_type)
          {
            case ListType.Numbered:
              result.Append($"{i + 1}. ");
              break;
            case ListType.Dotted:
              result.Append("- ");
              break;
            default:
              throw new ArgumentOutOfRangeException();
          }

          result.Append($"{item as string}\n");
        }
      }

      return result.ToString();
    }

    #endregion
  }
}