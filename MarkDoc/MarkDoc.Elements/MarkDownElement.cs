﻿namespace MarkDocElements
{
  /// <summary>
  /// Base type for all mark down elements
  /// </summary>
  public abstract class MarkDownElement
    : IMarkDownElement
  {
    /// <inheritdoc cref="IMarkDownElement.HeadingText"/>
    public string HeadingText { get; }

    /// <inheritdoc cref="IMarkDownElement.HeadingLevel"/>
    public int HeadingLevel { get; }

    /// <inheritdoc cref="IMarkDownElement.HasHeading"/>
    public bool HasHeading { get; }

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="headingText">Element heading text</param>
    /// <param name="headingLevel">Element heading level</param>
    protected MarkDownElement(string headingText, int headingLevel)
    {
      HeadingText = headingText;
      HeadingLevel = headingLevel;
      HasHeading = HeadingText.Trim() != string.Empty;
    }

    /// <inheritdoc cref="IMarkDownElement.ToString()"/>
    public override string ToString()
      => HasHeading
        ? $"{new string('#', HeadingLevel + 1)} {HeadingText}\n"
        : string.Empty;
  }
}