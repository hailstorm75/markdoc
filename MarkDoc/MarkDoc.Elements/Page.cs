﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MarkDocCommon;

namespace MarkDocElements
{
  /// <summary>
  /// Stack of <see cref="T:MarkDocElements.MarkDownElement" /> instances
  /// </summary>
  /// <inheritdoc />
  public sealed class Page
    : MarkDownElement
  {
    #region Fields

    private readonly string m_pageDescription;
    private Lazy<Dictionary<bool, List<IMarkDownElement>>> m_lazyItemContent;

    #endregion

    #region Properties

    public IEnumerable<Page> SubPages
      => m_lazyItemContent.Value.ContainsKey(true)
        ? m_lazyItemContent.Value[true].Cast<Page>()
        : Enumerable.Empty<Page>();

    public IEnumerable<IMarkDownElement> Content
      => m_lazyItemContent.Value.ContainsKey(false)
        ? m_lazyItemContent.Value[false]
        : Enumerable.Empty<IMarkDownElement>();

    #endregion

    /// <inheritdoc />
    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="headingText">Element heading text</param>
    /// <param name="pageDescription">Description of page content</param>
    public Page(string headingText, string pageDescription = "")
      : base(headingText, 0)
      => m_pageDescription = pageDescription;

    public Page SetContent(IEnumerable<IMarkDownElement> items)
    {
      m_lazyItemContent = new Lazy<Dictionary<bool, List<IMarkDownElement>>>(() => items.GroupBy(x => x is Page)
        .ToDictionary(x => x.Key, x => x.Select(y => y)
          .ToList()));

      return this;
    }

    private void FillTableOfContents(ref ItemList tableOfContents, string heading)
    {
      foreach (var page in SubPages.OrderBy(x => x.HeadingText, StringComparer.OrdinalIgnoreCase))
      {
        tableOfContents.Add(page.HeadingText.ToLink(
          $"{heading}/{page.HeadingText.Replace(" ", "-").Replace("<", "").Replace(">", "").Replace(".", "")}"));
        var subContent = page.GetTableOfContents(heading);
        if (subContent == null) continue;

        tableOfContents.Add(subContent);
      }
    }

    private ItemList GetTableOfContents(string parentHeading = "")
    {
      if (!SubPages.Any()) return null;

      var tableOfContents = new ItemList(ItemList.ListType.Dotted);
      FillTableOfContents(ref tableOfContents, $"{parentHeading}/{HeadingText.Replace(" ", "-").Replace(".", "")}");

      return tableOfContents;
    }

    /// <inheritdoc cref="MarkDownElement.ToString()" />
    public override string ToString()
    {
      var result = new StringBuilder();
      if (HasHeading)
        result.Append(base.ToString());

      if (!string.IsNullOrEmpty(m_pageDescription?.Trim()))
        result.AppendLine(m_pageDescription);

      if (SubPages.Any())
      {
        var tableOfContents =
          new ItemList(ItemList.ListType.Dotted, Constants.SECTION_TABLE_OF_CONTENTS, HeadingLevel + 1);
        FillTableOfContents(ref tableOfContents, HeadingText.Replace(" ", "-").Replace(".", ""));

        result.Append(tableOfContents);
      }

      foreach (var markDownElement in Content)
        result.AppendLine(markDownElement.ToString());

      return result.ToString();
    }
  }
}