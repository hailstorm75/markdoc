﻿using System.Collections.Generic;
using System.Text;

namespace MarkDocElements
{
  public class Section
    : MarkDownElement
  {
    private readonly List<object> m_content;
    public int ContentCount => m_content.Count;

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="headingText">Element heading text</param>
    /// <param name="headingLevel">Element heading level</param>
    public Section(string headingText, int headingLevel)
      : base(headingText, headingLevel)
    {
      m_content = new List<object>();
    }

    public Section Add(IMarkDownElement element)
    {
      if (element is Page)
        return this;

      m_content.Add(element);
      return this;
    }

    public Section Add(string text)
    {
      m_content.Add(text);
      return this;
    }

    public override string ToString()
    {
      var result = new StringBuilder();
      if (HasHeading)
        result.AppendLine(base.ToString());

      foreach (var element in m_content)
        result.AppendLine(element.ToString()).AppendLine();

      return result.ToString();
    }
  }
}