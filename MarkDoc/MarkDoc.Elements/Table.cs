﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarkDocElements
{
  /// <summary>
  /// Markdown element representing a table
  /// </summary>
  public sealed class Table
    : MarkDownElement
  {
    #region Fields

    private readonly IComparer<string[]> m_comparer;
    private const string DEL_VERTICAL = "|";
    private const string DEL_HORIZONTAL = "-";

    #endregion

    #region Properties

    /// <summary>
    ///
    /// </summary>
    public IReadOnlyCollection<string> Headings { get; }

    /// <summary>
    ///
    /// </summary>
    public int Columns => Headings.Count;

    /// <summary>
    ///
    /// </summary>
    public List<string[]> Rows { get; private set; }

    #endregion

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="headings">Table headings</param>
    /// <param name="comparer">Comparer for table row sorting</param>
    /// <param name="headingText">Element heading text</param>
    /// <param name="headingLevel">Element heading level</param>
    public Table(IReadOnlyCollection<string> headings, IComparer<string[]> comparer = null, string headingText = "",
      int headingLevel = 0)
      : base(headingText, headingLevel)
    {
      m_comparer = comparer;
      Headings = headings;
      Rows = new List<string[]>();
    }

    #region Methods

    /// <summary>
    /// Add row to table
    /// </summary>
    /// <param name="rowItems">Row content</param>
    /// <exception cref="ArgumentException"></exception>
    public void AddRow(IReadOnlyCollection<string> rowItems)
    {
      if (rowItems.Count > Columns)
        throw new ArgumentException($"Number of items in {nameof(rowItems)} is greater than {Columns} count.");

      Rows.Add(CreateRow(rowItems));
    }

    private string[] CreateRow(IReadOnlyCollection<string> rowItem)
    {
      var row = new string[Columns];

      for (var i = 0; i < rowItem.Count; i++)
        row[i] = rowItem.ElementAt(i);

      return row;
    }

    /// <inheritdoc cref="MarkDownElement.ToString()" />
    public override string ToString()
    {
      var result = new StringBuilder();
      result.Append(base.ToString());

      // Column headers
      result.Append(DEL_VERTICAL);
      foreach (var heading in Headings)
        result.Append(" ").Append(heading).Append(" ").Append(DEL_VERTICAL);

      // Horizontal line
      result.Append("\n").Append(DEL_VERTICAL);
      for (var i = 0; i < Columns; i++)
        result.Append(" ")
          .Append(DEL_HORIZONTAL)
          .Append(DEL_HORIZONTAL)
          .Append(DEL_HORIZONTAL)
          .Append(" ")
          .Append(DEL_VERTICAL);

      // Rows
      var rows = m_comparer != null
        ? Rows.OrderBy(x => x, m_comparer)
        : Rows.AsEnumerable();
      foreach (var row in rows)
      {
        result.Append("\n").Append(DEL_VERTICAL);

        foreach (var item in row)
          result.Append(" ").Append(item).Append(" ").Append(DEL_VERTICAL);
      }

      return result.ToString();
    }

    #endregion
  }
}