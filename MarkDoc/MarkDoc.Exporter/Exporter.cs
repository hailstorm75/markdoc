﻿using System.IO;
using System.Linq;
using System.Threading;
using MarkDocCommon;
using MarkDocElements;

namespace MarkDocExporter
{
  public static class Exporter
  {
    public static void Export(Page toExport, string outputPath, CancellationToken cancellation)
    {
      cancellation.ThrowIfCancellationRequested();

      File.WriteAllText(Path.Combine(outputPath,
                                     $"{toExport.HeadingText.RemoveSpecial()}.{Constants.EXT_MARKDOWN}"),
                                     toExport.ToString());

      if (!toExport.SubPages.Any()) return;

      var path = Path.Combine(outputPath, toExport.HeadingText.RemoveSpecial());
      Directory.CreateDirectory(path);
      foreach (var page in toExport.SubPages)
        Export(page, path, cancellation);
    }
  }
}