﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using MarkDocCommon;
using MarkDocElements;

namespace MarkDocGenerator
{
  public class DocRetriever
  {
    private readonly Assembly m_binary;
    private readonly List<XmlNode> m_documentation;

    public DocRetriever(Assembly binary, XmlNode documentation)
    {
      m_binary = binary;
      m_documentation = documentation?.SelectNodes("doc/members/member")?.Cast<XmlNode>().ToList();
    }

    /// <summary>
    /// Finds given <paramref name="memberInfo"/> in documentation
    /// </summary>
    /// <param name="memberInfo">Type to find</param>
    /// <returns>Node containing documentation</returns>
    private XmlNode FindInDoc(MemberInfo memberInfo)
    {
      string toSearch;

      switch (memberInfo)
      {
        case Type type:
          toSearch = $"T:{type.FullName?.Replace('+', '.')}";
          break;
        case ConstructorInfo constructorInfo:
          {
            var @params = constructorInfo.GetParameters();
            var paramsString = CreateParamString(@params);

            toSearch = $"M:{constructorInfo.DeclaringType.Namespace}.{constructorInfo.DeclaringType.Name}.#ctor{paramsString}";
            break;
          }
        case MethodBase methodBase:
          {
            var @params = methodBase.GetParameters();
            var paramsString = CreateParamString(@params);

            toSearch = $"M:{methodBase.DeclaringType.Namespace}.{methodBase.DeclaringType.Name}.{methodBase.Name}{paramsString}";
            break;
          }
        case FieldInfo fieldInfo:
          toSearch = $"F:{fieldInfo.FieldType.FullName.Replace('+', '.')}.{fieldInfo.Name}";
          break;
        case PropertyInfo propertyInfo:
          toSearch = $"P:{propertyInfo.DeclaringType.Namespace}.{propertyInfo.DeclaringType.Name}.{propertyInfo.Name}";
          break;
        default:
          return null;
      }

      var element = m_documentation?.FirstOrDefault(x => x.Attributes?["name"].Value.Equals(toSearch) ?? false);

      return element;
    }

    /// <summary>
    /// Searches for the source of the inherited documentation by <paramref name="memberInfo"/>
    /// </summary>
    /// <param name="memberInfo"></param>
    /// <param name="cref">Base type name</param>
    /// <returns>Inherited type</returns>
    private MemberInfo FindInheritedDocSource(MemberInfo memberInfo, string cref = "")
    {
      if (memberInfo is Type type)
        return type.GetInterfaces().FirstOrDefault() ?? type.BaseType;

      var declaringType = memberInfo.DeclaringType;
      if (declaringType == null)
        return null;
      var inheritanceList = new[] { declaringType.BaseType }.Union(declaringType.GetInterfaces())
        .Where(x => x != null && x.Assembly == m_binary);

      MemberInfo result = null;
      foreach (var inheritance in inheritanceList)
      {
        switch (memberInfo.MemberType)
        {
          case MemberTypes.Constructor:
            result = inheritance.GetConstructors().FirstOrDefault(x => x.Equals(memberInfo as ConstructorInfo));
            break;
          case MemberTypes.Event:
            // TODO Doesn't work
            result = inheritance.GetEvents().FirstOrDefault(x => x.Equals(memberInfo as EventInfo));
            break;
          case MemberTypes.Field:
            result = inheritance.GetFields().FirstOrDefault(x => x.Name.Equals(memberInfo.Name));
            break;
          case MemberTypes.Method:
            var method = (MethodInfo)memberInfo;
            var methodParams = method.GetParameters();
            result = inheritance.GetMethods()
              .FirstOrDefault(x =>
              {
                if (!x.Name.Equals(method.Name))
                  return false;

                var xParams = x.GetParameters();
                if (xParams.Length != methodParams.Length)
                  return false;
                if (xParams.Length == 0)
                  return true;

                return !xParams.Where((t, i) => t.ParameterType != methodParams[i].ParameterType).Any();
              });

            break;
          case MemberTypes.Property:
            result = inheritance.GetProperties().FirstOrDefault(x => x.Name.Equals(memberInfo.Name));
            break;
          case MemberTypes.NestedType:
            // TODO Doesn't work
            result = inheritance.GetNestedTypes().FirstOrDefault(x => x.Equals(memberInfo));
            break;
        }

        if (result != null)
          break;
      }

      return result;
    }

    /// <summary>
    /// Helper method which converts method parameters to documentation format
    /// </summary>
    /// <param name="params">Parameters to convert</param>
    /// <returns>Conversion result</returns>
    private static string CreateParamString(ParameterInfo[] @params)
    {
      var paramsString = string.Empty;
      if (@params.Any())
      {
        paramsString = string.Concat(
          "(",
          string.Join(",", @params.Select(param =>
          {
            var result = new StringBuilder();
            if (param.IsOut) result.Append("out ");
            else if (param.IsIn) result.Append("in ");
            else if (param.ParameterType.IsByRef) result.Append("ref ");

            if (param.ParameterType.IsGenericParameter)
              result.Append("`0");
            else
            {
              var name = $"{param.ParameterType.Namespace}.{param.ParameterType.Name}";
              if (!param.ParameterType.IsArray
                  && param.ParameterType.ContainsGenericParameters)
              {
                result.Append(name.Substring(0, name.IndexOf("`", StringComparison.InvariantCultureIgnoreCase)));
                result.Append($"{{{string.Join(", ", param.ParameterType.GetGenericArguments().Select(y => "`0"))}}}");
              }
              else if (param.ParameterType.IsArray)
              {
                result.Append(param.ParameterType.ContainsGenericParameters
                  ? "`0"
                  : param.ParameterType.FullName.Substring(0, param.ParameterType.FullName.IndexOf("[")));

                var dimension = param.ParameterType.Name
                                  .Substring(param.ParameterType.Name.IndexOf("[", StringComparison.Ordinal))
                                  .Count(x => x == ',') + 1;

                result.Append(dimension != 1
                  ? $"[{string.Join(",", Enumerable.Repeat("0:", dimension))}]"
                  : "[]");
              }
              else
                result.Append(name);
            }

            return result.ToString();
          })),
          ")");
      }

      return paramsString;
    }

    /// <summary>
    /// Gets documentation for given <paramref name="memberInfo"/>
    /// </summary>
    /// <param name="memberInfo"></param>
    /// <param name="tag"></param>
    /// <returns></returns>
    public IEnumerable<XmlNode> GetDoc(MemberInfo memberInfo, string tag)
    {
      var node = FindInDoc(memberInfo)?.ChildNodes.Cast<XmlNode>().ToList();
      if (node == null)
        yield break;

      var searchingByTag = node.Where(x => x.Name.Equals(tag)).ToList();
      if (searchingByTag.Any())
      {
        foreach (var element in searchingByTag)
          yield return element;
        yield break;
      }

      var inheritDoc = node.FirstOrDefault(x => x.Name.Equals(Constants.TAG_INHERITDOC));
      if (inheritDoc == null)
        yield break;

      var inheritFrom = inheritDoc.Attributes?[Constants.ATTR_REFERENCE];
      foreach (var documentation in GetDoc(FindInheritedDocSource(memberInfo, inheritFrom?.Value ?? string.Empty), tag))
        yield return documentation;
    }

    public static string GetDocValue(XmlNode docTag, int level = 0)
    {
      if (docTag?.InnerText.Trim() == null)
        return string.Empty;

      var sb = new List<string>();
      foreach (XmlNode childNode in docTag.ChildNodes)
      {
        if (childNode.Name.Equals("#text", StringComparison.InvariantCultureIgnoreCase))
          sb.Add(childNode.Value.Replace("\r\n            ", "\n").Trim());
        else
        {
          switch (childNode.Name.ToLowerInvariant())
          {
            case Constants.TAG_SEE:
              //sb.Add(GetDocAttributeValue(childNode, Constants.ATTR_REFERENCE));
              break;
            case Constants.TAG_TYPEPARAMREF:
            case Constants.TAG_PARAMREF:
              sb.Add(GetDocAttributeValue(childNode, Constants.ATTR_NAME));
              break;
            case Constants.TAG_PARA:
              sb.Add($"<br><br>{GetDocValue(childNode, level)}");
              break;
            case Constants.TAG_CODE:
              sb.Add(GetDocValue(childNode, level).ToCode());
              break;
            case Constants.TAG_CODEBLOCK:
              sb.Add($"\n{GetDocValue(childNode, level).ToCodeBlock()}");
              break;
            case Constants.TAG_LIST:
              sb.Add(ProcessList(childNode, level));
              break;
          }
        }
      }

      return string.Join(" ", sb);
    }

    public static string ProcessList(XmlNode docTag, int level = 0)
    {
      var listType = GetDocAttributeValue(docTag, Constants.ATTR_TYPE);
      switch (listType)
      {
        case Constants.ATTR_VAL_TABLE:
          return CreateTable(docTag);
        case Constants.ATTR_VAL_NUMBER:
          return CreateList(docTag, true, level);
        case Constants.ATTR_VAL_BULLET:
        default:
          return CreateList(docTag, false, level);
      }
    }

    private static string CreateTable(XmlNode docTag)
    {
      var headings = docTag.ChildNodes.Cast<XmlNode>()
                                      .Where(x => x.Name.Equals(Constants.TAG_LISTHEADER, StringComparison.InvariantCultureIgnoreCase))
                                      .FirstOrDefault()
                                      ?.ChildNodes.Cast<XmlNode>()
                                      ?.Where(x => x.Name.Equals(Constants.TAG_TERM, StringComparison.InvariantCultureIgnoreCase))
                                      ?.Select(x => GetDocValue(x))
                                      ?.ToList();
      if (headings == null)
        return string.Empty; // TODO Log error

      var table = new Table(headings);
      foreach (var item in docTag.ChildNodes.Cast<XmlNode>().Where(x => x.Name.Equals(Constants.TAG_ITEM, StringComparison.InvariantCultureIgnoreCase)))
      {
        var cols = item.ChildNodes.Cast<XmlNode>()
                                  .Where(x => x.Name.Equals(Constants.TAG_TERM, StringComparison.InvariantCultureIgnoreCase))
                                  .Select(x => GetDocValue(x))
                                  .ToList();
        if (cols.Count < headings.Count)
          for (int i = 0; i < headings.Count - cols.Count; ++i)
            cols.Add(string.Empty);
        else if (cols.Count > headings.Count)
          cols.RemoveRange(headings.Count - 1, cols.Count - headings.Count);

        table.AddRow(cols);
      }

      return table.ToString();
    }

    private static string CreateList(XmlNode docTag, bool ordered, int level)
    {
      var sb = new StringBuilder();
      for (var i = 0; i < docTag.ChildNodes.Count; ++i)
      {
        XmlNode listHeader = null,
                term = null,
                description = null;

        var nodes = docTag.ChildNodes[i].ChildNodes.Cast<XmlNode>().ToArray();
        foreach (var node in nodes)
        {
          switch (node.Name.ToLowerInvariant())
          {
            case Constants.TAG_LISTHEADER:
              listHeader = string.IsNullOrEmpty(node.InnerXml)
                ? null
                : node;
              break;
            case Constants.TAG_TERM:
              term = string.IsNullOrEmpty(node.InnerXml)
                ? null
                : node;
              break;
            case Constants.TAG_DESCRIPTION:
              description = string.IsNullOrEmpty(node.InnerXml)
                ? null
                : node;
              break;
          }
        }

        if (listHeader != null)
        {
          sb.AppendLine(TextFormatter.ToBold(GetDocValue(listHeader)));
          if (description != null)
            sb.AppendLine(TextFormatter.ToBlockQuote(GetDocValue(listHeader)));

          continue;
        }

        if (term == null && description == null)
          continue;

        if (level != 0)
          sb.Append(new string(' ', level * 2));

        if (ordered)
          sb.Append($"{i + 1}. ");
        else
          sb.Append("- ");

        if (term != null)
        {
          sb.Append(TextFormatter.ToBold(GetDocValue(term, level + 1)));
          if (description != null)
            sb.Append(": ");
        }

        if (description != null)
          sb.Append(GetDocValue(description, level + 1));

        if (i != docTag.ChildNodes.Count - 1)
          sb.AppendLine();
      }

      return sb.ToString();
    }

    public static string GetDocAttributeValue(XmlNode docTag, string attribute)
      => docTag?[attribute]?.InnerText ??
        docTag.Attributes.Cast<XmlAttribute>().FirstOrDefault(x => x.Name.Equals(attribute))?.Value ??
        string.Empty;
  }
}