﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml;
using MarkDocCommon;
using MarkDocElements;

namespace MarkDocGenerator
{
  /// <summary>
  /// Markdown generator
  /// </summary>
  public class Generator
  {
    #region Fields

    private readonly DocRetriever m_doc;

    private readonly List<Func<MethodBase, IMarkDownElement>> m_sectionFactory;

    #endregion

    #region Methods

    /// <summary>
    /// Default constructor
    /// </summary>
    /// <param name="binary"></param>
    /// <param name="documentation"></param>
    private Generator(Assembly binary, XmlNode documentation)
    {
      m_doc = new DocRetriever(binary, documentation);
      m_sectionFactory = new List<Func<MethodBase, IMarkDownElement>>
      {
        GetDescriptionSection,
        GetRemarksSection,
        GetListSection,
        GetParametersSection,
        GetExceptionsSection,
        GetExamplesSection,
        GetReturnsSection
      };
    }

    /// <summary>
    /// Generates markdown page elements based on <paramref name="binary"/> and optionally <paramref name="documentation"/>
    /// </summary>
    /// <param name="binary"></param>
    /// <param name="documentation"></param>
    /// <returns>Markdown pages</returns>
    public static IEnumerable<Page> Generate(Assembly binary, XmlDocument documentation = null)
    {
      var generator = new Generator(binary, documentation);
      var types = binary.GetExportedTypes();
      var namespaces = types.GroupBy(x => x.Namespace);

      foreach (var ns in namespaces)
      {
        var namespacePage = new Page(ns.Key);
        namespacePage.SetContent(ns.Select(generator.TypeToPage));

        yield return namespacePage;
      }
    }

    #endregion

    #region Page generators

    private Page TypeToPage(Type type)
    {
      if (type == null)
        throw new ArgumentNullException();

      var page = new Page(NameResolver.FixTypeName(type.FullName.Substring(type.Namespace.Length + 1), type),
        DocRetriever.GetDocValue(m_doc.GetDoc(type, Constants.TAG_SUMMARY).FirstOrDefault()).Trim().ToBlockQuote());
      page.SetContent(GeneratePageContent(type));
      return page;
    }

    private IEnumerable<IMarkDownElement> GeneratePageContent(Type type)
    {
      if (type.IsEnum)
        foreach (var element in GenerateEnumPage(type))
          yield return element;
      else
        foreach (var element in GenerateClassPage(type))
          yield return element;
    }

    private IEnumerable<IMarkDownElement> GenerateEnumPage(Type type)
    {
      var enumTypes = type.GetFields().Skip(1);

      var table = new Table(new[]
      {
        Constants.LABEL_NAME,
        Constants.LABEL_DESCRIPTION
      }, headingText: "Enumerable values", headingLevel: 1);
      foreach (var enumType in enumTypes)
        table.AddRow(new[]
        {
          enumType.Name,
          DocRetriever.GetDocValue(m_doc.GetDoc(enumType, Constants.TAG_SUMMARY).FirstOrDefault())
        });

      yield return table;
    }

    private IEnumerable<IMarkDownElement> GenerateClassPage(Type type)
    {
      var nestedTypes = type.GetNestedTypes();

      var nested = GenerateNestedTypesTable(nestedTypes, type);
      if (nested != null)
        yield return nested;

      var enums = GenerateEnumTable(nestedTypes, type);
      if (enums != null)
        yield return enums;

      var methods = type.GetMethods()
        .Where(x => !x.Name.StartsWith("get_") && !x.Name.StartsWith("set_"))
        .GroupBy(x => x.Name)
        .ToDictionary(x => x.Key, x => x.ToList());
      foreach (var markDownElement in GenerateMethodTables(methods))
        yield return markDownElement;

      var properties = type.GetProperties();
      if (properties.Any())
        yield return GeneratePropertyTable(properties, Constants.SECTION_PROPERTIES);

      var detailedDescription = GenerateDetailedDescription(type);
      if (detailedDescription != null)
        yield return detailedDescription;

      var constructorDocumentation = GenerateConstructorDocumentation(type.GetConstructors());
      if (constructorDocumentation != null)
        yield return constructorDocumentation;

      var methodDocumentation = GenerateMethodDocumentation(methods);
      if (methodDocumentation != null)
        yield return methodDocumentation;
    }

    private IMarkDownElement GenerateMethodDocumentation(Dictionary<string, List<MethodInfo>> methods)
    {
      if (!methods.Any()) return null;

      var methodDocumentation = new Section(Constants.SECTION_METHOD_DOCUMENTATION, 1);
      foreach (var method in methods)
      {
        if (method.Value.Count > 1)
          for (var i = 0; i < method.Value.Count; i++)
            methodDocumentation.Add(GenerateMethodSection(method.Value[i],
              new Pair<int, int>(i + 1, method.Value.Count),
              m_sectionFactory));
        else
          methodDocumentation.Add(GenerateMethodSection(method.Value[0], new Pair<int, int>(1, 1), m_sectionFactory));
      }

      return methodDocumentation;
    }

    private IMarkDownElement GenerateConstructorDocumentation(ConstructorInfo[] constructors)
    {
      if (!constructors.Any()) return null;

      var methodDocumentation = new Section(Constants.SECTION_CONSTRUCTORS, 1);
      if (constructors.Length > 1)
        for (var i = 0; i < constructors.Length; i++)
          methodDocumentation.Add(GenerateMethodSection(constructors[i], new Pair<int, int>(i + 1, constructors.Length),
            m_sectionFactory));
      else
        methodDocumentation.Add(GenerateMethodSection(constructors[0], new Pair<int, int>(1, 1), m_sectionFactory));

      return methodDocumentation;
    }

    private static IMarkDownElement GenerateNestedTypesTable(Type[] nestedTypes, Type type)
    {
      var classes = nestedTypes.Where(x => x.IsClass)
        .Select(x => new[] { x.FullName.Substring(type.FullName.Length) })
        .ToList();
      if (!classes.Any()) return null;

      var classTable = new Table(new[] { Constants.SECTION_CLASS_NAME }, headingText: Constants.SECTION_CLASSES,
        headingLevel: 1);
      foreach (var @class in classes)
        classTable.AddRow(@class);
      return classTable;
    }

    private static IMarkDownElement GenerateEnumTable(IEnumerable<Type> nestedTypes, Type type)
    {
      var enums = nestedTypes.Where(x => x.IsEnum)
        .Select(x => new[] { x.FullName.Substring(type.FullName.Length + 1) })
        .ToList();
      if (!enums.Any()) return null;

      var enumTable = new Table(new[] { Constants.SECTION_ENUM_NAME }, headingText: Constants.SECTION_ENUMS,
        headingLevel: 1);
      foreach (var @enum in enums)
        enumTable.AddRow(@enum);

      return enumTable;
    }

    private Section GenerateDetailedDescription(Type type)
    {
      var section = new Section(Constants.SECTION_LONG_DESCRIPTION, 1);

      // Description
      var description = DocRetriever.GetDocValue(m_doc.GetDoc(type, Constants.TAG_SUMMARY).FirstOrDefault()).Trim();
      if (description.Trim() != string.Empty)
        section.Add(description.ToBlockQuote());

      // Inheritance
      var @base = type.BaseType;
      var interfaces = type.GetInterfaces();
      if (@base != null && @base != typeof(object)
          || interfaces.Length > 0)
      {
        var tableInheritance =
          new Table(new[] { "Class/Interface" }, headingText: Constants.SECTION_INHERITANCE, headingLevel: 2);
        if (@base != null
            && @base != typeof(object))
          tableInheritance.AddRow(new[] { NameResolver.FixTypeName(@base.Name, @base) });
        foreach (var @interface in interfaces)
          tableInheritance.AddRow(new[] { NameResolver.FixTypeName(@interface.Name, @interface) });

        section.Add(tableInheritance);
      }

      // Generics
      var genericTypes = type.GetGenericArguments();
      if (genericTypes.Any())
      {
        var genericTypesDocs = m_doc.GetDoc(type, Constants.TAG_TYPEPARAM)
          .ToDictionary(x => DocRetriever.GetDocAttributeValue(x, Constants.ATTR_NAME), x => DocRetriever.GetDocValue(x));

        // Generic types
        foreach (var genericType in genericTypes)
        {
          var tableGenericParam = new Table(new[]
            {
              Constants.LABEL_TYPE,
              Constants.LABEL_DESCRIPTION
            },
            headingText: Constants.SECTION_TEMPLATE_PARAMS, headingLevel: 2);
          tableGenericParam.AddRow(new[]
          {
            genericType.Name,
            genericTypesDocs.ContainsKey(genericType.Name) ? genericTypesDocs[genericType.Name] : string.Empty
          });

          section.Add(tableGenericParam);
        }

        // Generic constraints
        foreach (var genericType in genericTypes)
        {
          var constraints = genericType.GetGenericParameterConstraints();
          if (!constraints.Any()) continue;

          var tableGenericConstraints = new Table(new[] { Constants.LABEL_CONSTRAINT });
          foreach (var constraint in constraints)
            tableGenericConstraints.AddRow(new[]
              {$"{genericType.Name} : {NameResolver.FixTypeName(constraint.Name, constraint).EscapeSpecial()}"});

          section.Add(tableGenericConstraints);
        }
      }

      return section.ContentCount > 0 ? section : null;
    }

    #endregion

    #region Method generators

    private IEnumerable<IMarkDownElement> GenerateMethodTables(Dictionary<string, List<MethodInfo>> methods)
    {
      if (!methods.Any()) yield break;

      var publicMethods = methods.Where(x => x.Value.First().IsPublic)
        .GroupBy(x => x.Value.First().IsStatic)
        .ToDictionary(x => x.Key, x => x.Select(y => new Pair<MethodInfo, int>(y.Value.First(), y.Value.Count)));
      // Public
      if (publicMethods.ContainsKey(false)
          && publicMethods[false].Any())
        yield return GenerateMethodTable(publicMethods[false], Constants.SECTION_PUBLIC_METHODS);

      // Public Static
      if (publicMethods.ContainsKey(true)
          && publicMethods[true].Any())
        yield return GenerateMethodTable(publicMethods[true], Constants.SECTION_STATIC_PUBLIC_METHODS);

      // -----------------------------------------------------------------------------------------------------

      var protectedMethods = methods.Where(x => x.Value.First().IsFamily)
        .GroupBy(x => x.Value.First().IsStatic)
        .ToDictionary(x => x.Key, x => x.Select(y => new Pair<MethodInfo, int>(y.Value.First(), y.Value.Count)));

      // Protected
      if (protectedMethods.ContainsKey(false)
          && protectedMethods[false].Any())
        yield return GenerateMethodTable(protectedMethods[false], Constants.SECTION_PROTECTED_METHODS);

      // Protected Static
      if (protectedMethods.ContainsKey(true)
          && protectedMethods[true].Any())
        yield return GenerateMethodTable(protectedMethods[true], Constants.SECTION_STATIC_PROTECTED_METHODS);
    }

    private Table GenerateMethodTable(IEnumerable<Pair<MethodInfo, int>> pairs, string heading)
    {
      var methodTable = new Table(new[]
      {
        Constants.LABEL_TYPE,
        Constants.LABEL_NAME
      }, headingText: heading, headingLevel: 1);
      foreach (var pair in pairs)
      {
        var keywords = BuildMethodKeywords(pair.ItemA);

        methodTable.AddRow(new[]
        {
          $"{string.Join(" ", keywords)} {NameResolver.FixTypeName(pair.ItemA.ReturnType.Name, pair.ItemA.ReturnType)}"
            .ToCode(),
          TextFormatter.ToMultiline(BuildMethodName(pair.ItemA, true, pair.ItemB > 1),
            DocRetriever.GetDocValue(m_doc.GetDoc(pair.ItemA, Constants.TAG_SUMMARY).FirstOrDefault()))
        });
      }

      return methodTable;
    }

    private static Section GenerateMethodSection(MethodBase method, Pair<int, int> overloadCount,
      IEnumerable<Func<MethodBase, IMarkDownElement>> sectionFactory)
    {
      var asyncKeyword = string.Empty;
      if ((AsyncStateMachineAttribute)method.GetCustomAttribute(typeof(AsyncStateMachineAttribute)) != null)
        asyncKeyword = "async ";

      var name = method.IsConstructor
        ? NameResolver.FixTypeName(method.DeclaringType.Name, method.DeclaringType)
        : NameResolver.FixMethodName(method.Name);
      if (overloadCount.ItemB != 1)
        name = $"{name} [{overloadCount.ItemA}/{overloadCount.ItemB}]";

      var section = new Section(name, 2);
      if (method is MethodInfo methodInfo)
        section.Add(
          $"{NameResolver.FixTypeName(methodInfo.ReturnType.Name, methodInfo.ReturnType)} {asyncKeyword}{BuildMethodName(method, false)}"
            .ToCodeBlock());
      else
        section.Add(BuildMethodName(method, false).ToCodeBlock());

      foreach (var func in sectionFactory)
      {
        var result = func(method);
        if (result != null) section.Add(result);
      }

      return section;
    }

    private IMarkDownElement GetDescriptionSection(MethodBase method)
    {
      var description = DocRetriever.GetDocValue(m_doc.GetDoc(method, Constants.TAG_SUMMARY).FirstOrDefault());
      if (string.IsNullOrEmpty(description)) return null;

      var descriptionSection = new Section(string.Empty, 0);
      descriptionSection.Add(description.ToBlockQuote());

      return descriptionSection;
    }

    private IMarkDownElement GetRemarksSection(MethodBase method)
    {
      var remarks = DocRetriever.GetDocValue(m_doc.GetDoc(method, Constants.TAG_REMARKS).FirstOrDefault());
      if (string.IsNullOrEmpty(remarks)) return null;

      var remarksSection = new Section(Constants.LABEL_REMARKS, 3);
      remarksSection.Add(remarks);

      return remarksSection;
    }

    private IMarkDownElement GetListSection(MethodBase method)
    {
      var list = DocRetriever.GetDocValue(m_doc.GetDoc(method, Constants.TAG_LIST).FirstOrDefault());

      var listSection = new Section(string.Empty, 3);
      listSection.Add(list);

      return listSection;
    }

    private IMarkDownElement GetParametersSection(MethodBase method)
    {
      var parameters = method.GetParameters();
      var parameterDocs = m_doc.GetDoc(method, Constants.TAG_PARAM)
        .Select(DocRetriever.GetDocValue)
        .Zip(parameters, (c, p) => new
        {
          p.Name,
          Comment = c
        })
        .ToList();

      if (!parameterDocs.Any()) return null;

      var parameterTable = new Table(new[]
      {
        Constants.LABEL_NAME,
        Constants.LABEL_DESCRIPTION
      }, headingText: Constants.LABEL_PARAMETERS, headingLevel: 3);
      foreach (var parameter in parameterDocs)
        parameterTable.AddRow(new[]
        {
          parameter.Name,
          parameter.Comment ?? string.Empty
        });

      return parameterTable;
    }

    private IMarkDownElement GetExceptionsSection(MethodBase method)
    {
      var exceptions = m_doc.GetDoc(method, Constants.TAG_EXCEPTION)
        .Select(x => DocRetriever.GetDocAttributeValue(x, Constants.ATTR_REFERENCE))
        .Where(x => !string.IsNullOrEmpty(x))
        .Select(x => x.Substring(2))
        .ToList();

      if (!exceptions.Any()) return null;

      var exceptionTable = new Table(new[] { Constants.LABEL_NAME }, headingText: Constants.LABEL_EXCEPTIONS,
        headingLevel: 3);
      foreach (var exception in exceptions)
        exceptionTable.AddRow(new[] { exception });
      return exceptionTable;
    }

    private IMarkDownElement GetExamplesSection(MethodBase method)
    {
      var examples = m_doc.GetDoc(method, Constants.TAG_EXAMPLE).Select(DocRetriever.GetDocValue).ToList();
      if (!examples.Any())
        return null;

      var examplesSection = new Section(string.Empty, 0);
      if (examples.Count == 1)
      {
        var example = new Section(Constants.LABEL_EXAMPLE, 3);
        example.Add(examples.First());

        examplesSection.Add(example);
      }
      else
        for (var i = 0; i < examples.Count; i++)
        {
          var example = new Section($"{Constants.LABEL_EXAMPLE} [{i + 1}/{examples.Count}]", 3);
          example.Add(examples[i]);

          examplesSection.Add(example);
        }

      return examplesSection;
    }

    private IMarkDownElement GetReturnsSection(MethodBase method)
    {
      var returns = m_doc.GetDoc(method, Constants.TAG_RETURNS)
        .Select(DocRetriever.GetDocValue)
        .ToList();

      if (!returns.Any()
          || string.IsNullOrEmpty(returns[0])) return null;

      var returnsTable = new Table(new[] { Constants.LABEL_DESCRIPTION }, headingText: Constants.LABEL_RETURNS,
        headingLevel: 3);
      returnsTable.AddRow(returns);

      return returnsTable;
    }

    private static IEnumerable<string> BuildMethodKeywords(MethodBase method)
    {
      if (method.IsStatic)
        yield return "static";
      else if (method.DeclaringType == method.ReflectedType
               && !method.IsAbstract)
        yield return "override";
      else if (method.IsAbstract) // TODO Skip if inherited from interface
        yield return "abstract";
      else if (method.IsVirtual) // TODO Skip if inherited from interface
        yield return "virtual";
    }

    private static string BuildMethodName(MethodBase method, bool applyFormatting, bool hasOverrides = false)
    {
      var name = method.IsConstructor
        ? NameResolver.FixTypeName(method.DeclaringType.Name, method.DeclaringType)
        : NameResolver.FixMethodName(method.Name);
      var parameters = hasOverrides
        ? new[] { "..." }
        : method.GetParameters()
          .Select(x =>
          {
            var typeName = NameResolver.FixTypeName(x.ParameterType.Name, x.ParameterType);
            return $"{(applyFormatting ? typeName.EscapeSpecial() : typeName)} {x.Name}";
          });

      return applyFormatting
        ? $"{name.ToBold().ToLink(string.Empty)}({string.Join(", ", parameters)})"
        : $"{name}({string.Join(", ", parameters)})";
    }

    #endregion

    #region Property generators

    private Table GeneratePropertyTable(IEnumerable<PropertyInfo> properties, string heading)
    {
      var propertyTable = new Table(new[]
      {
        Constants.LABEL_TYPE,
        Constants.LABEL_NAME
      }, headingText: heading, headingLevel: 1);
      foreach (var property in properties)
        propertyTable.AddRow(new[]
        {
          NameResolver.FixTypeName(property.PropertyType.Name, property.PropertyType).ToCode(),
          TextFormatter.ToMultiline($"{property.Name.ToBold()} [{BuildPropertyAccessors(property).ToItalic()}]",
            DocRetriever.GetDocValue(m_doc.GetDoc(property, Constants.TAG_SUMMARY).FirstOrDefault()).Trim())
        });
      return propertyTable;
    }

    private static string BuildPropertyAccessors(PropertyInfo property)
    {
      var getset = new List<string>();
      if (property.CanRead)
        getset.Add("get");
      if (property.CanWrite)
        getset.Add("set");

      return string.Join(", ", getset);
    }

    #endregion
  }
}