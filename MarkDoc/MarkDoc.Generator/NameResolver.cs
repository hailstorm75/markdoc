﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace MarkDocGenerator
{
  public static class NameResolver
  {
    private static readonly Dictionary<string, string> OPERATORS = new Dictionary<string, string>
    {
      {"op_Implicit", ""},
      {"op_Explicit", ""},
      {"op_Addition", "+"},
      {"op_Subtraction", "-"},
      {"op_Multiply", "*"},
      {"op_Division", "/"},
      {"op_Modulus", "%"},
      {"op_ExclusiveOr", "^"},
      {"op_BitwiseAnd", "&"},
      {"op_BitwiseOr", "|"},
      {"op_LogicalAnd", "&&"},
      {"op_LogicalOr", "||"},
      {"op_LogicalNot", "!"},
      {"op_Assign", "="},
      {"op_LeftShift", "<<"},
      {"op_RightShift", ">>"},
      {"op_SignedRightShift", ""},
      {"op_UnsignedRightShift", ""},
      {"op_Equality", "=="},
      {"op_GreaterThan", ">"},
      {"op_LessThan", "<"},
      {"op_Inequality", "!="},
      {"op_GreaterThanOrEqual", ">="},
      {"op_LessThanOrEqual", "<="},
      {"op_MultiplicationAssignment", "*="},
      {"op_SubtractionAssignment", "-="},
      {"op_ExclusiveOrAssignment", "^="},
      {"op_LeftShiftAssignment", "<<="},
      {"op_ModulusAssignment", "%="},
      {"op_AdditionAssignment", "+="},
      {"op_BitwiseAndAssignment", "&="},
      {"op_BitwiseOrAssignment", "|="},
      {"op_Comma", ","},
      {"op_DivisionAssignment", "/="},
      {"op_Decrement", "--"},
      {"op_Increment", "++"},
      {"op_UnaryNegation", "-"},
      {"op_UnaryPlus", "+"},
      {"op_OnesComplement", "~"}
    };

    private static readonly Dictionary<string, string> PRIMITIVES = new Dictionary<string, string>
    {
      {"Char", "char"},
      {"String", "string"},
      {"Boolean", "bool"},
      {"Int16", "short"},
      {"Int32", "int"},
      {"Int64", "long"},
      {"UInt16", "ushort"},
      {"UInt32", "uint"},
      {"UInt64", "ulong"},
      {"Single", "float"},
      {"Double", "double"},
      {"Byte", "byte"},
      {"SByte", "sbyte"},
      {"Decimal", "decimal"},
      {"Object", "object"},
      {"Void", "void"},
    };

    private const string GENERIC_PATTERN = "`[1-9]";

    public static string FixTypeName(string name, Type origin)
    {
      var regex = new Regex(GENERIC_PATTERN);
      return regex.IsMatch(name)
        ? Regex.Replace(name, GENERIC_PATTERN,
            $"<{string.Join(", ", origin.GetGenericArguments().Select(x => FixTypeName(x.Name, x)))}>")
          .Replace('+', '.')
        : ResolveTypeName(name);
    }

    private static string ResolveTypeName(string name)
      => PRIMITIVES.ContainsKey(name)
        ? $"{PRIMITIVES[name]}"
        : name;

    public static string FixMethodName(string name)
    {
      if (name.StartsWith("op_"))
        if (OPERATORS.ContainsKey(name))
          return $"operator {OPERATORS[name]}";
      return name;
    }
  }
}