﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using MarkDoc.Annotations;

namespace MarkDoc.Helpers
{
  /// <inheritdoc />
  public class NotifyPropertyChanged
    : INotifyPropertyChanged
  {
    /// <inheritdoc />
    public event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Notify UI that a given <paramref name="property"/> has changed
    /// </summary>
    /// <param name="property">Property name which has changed</param>
    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string property
      = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
  }
}