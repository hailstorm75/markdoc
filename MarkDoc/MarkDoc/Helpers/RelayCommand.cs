﻿using System;
using System.Windows.Input;

namespace MarkDoc.Helpers
{
  public class RelayCommand
    : ICommand
  {
    private readonly Action m_action;

    public event EventHandler CanExecuteChanged;

    public RelayCommand(Action action)
      => m_action = action;

    bool ICommand.CanExecute(object parameter)
      => true;

    void ICommand.Execute(object parameter)
      => m_action();
  }

  public class RelayCommand<T>
    : ICommand
  {
    private readonly Action<T> m_action;

    public event EventHandler CanExecuteChanged;

    public RelayCommand(Action<T> action)
      => m_action = action;

    bool ICommand.CanExecute(object parameter)
      => true;

    void ICommand.Execute(object parameter)
      => m_action((T) parameter);
  }
}