﻿using System.IO;
using MarkDoc.Helpers;

namespace MarkDoc.Models
{
  /// <summary>
  /// Wrapper for path to binary file and its documentation
  /// </summary>
  public class BinaryPath
    : NotifyPropertyChanged
  {
    #region Fields

    private string m_pathToBinary;
    private string m_pathToDocumentation;

    #endregion

    #region Properties

    public string PathToBinary
    {
      get => m_pathToBinary;
      set
      {
        m_pathToBinary = value;
        OnPropertyChanged(nameof(PathToBinary));
        OnPropertyChanged(nameof(BinaryFilename));
      }
    }

    public string PathToDocumentation
    {
      get => m_pathToDocumentation;
      set
      {
        m_pathToDocumentation = value;
        OnPropertyChanged(nameof(PathToDocumentation));
        OnPropertyChanged(nameof(DocumentationLinked));
      }
    }

    public string BinaryFilename
      => Path.GetFileNameWithoutExtension(m_pathToBinary);

    public bool DocumentationLinked
      => !string.IsNullOrEmpty(PathToDocumentation);

    #endregion
  }
}