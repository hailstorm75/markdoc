﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml;
using MarkDoc.Helpers;
using MarkDoc.Models;
using MarkDocCommon;
using MarkDocGenerator;
using MarkDocElements;
using MarkDocExporter;
using System.Windows.Forms;

namespace MarkDoc.ViewModels
{
  /// <summary>
  /// ViewModel for the MainWindowView
  /// </summary>
  public class MainWindowViewModel
    : BaseViewModel
  {
    #region Fields

    private string m_pathToOutput;
    private CancellationTokenSource m_cancellation;
    private ObservableCollection<BinaryPath> m_binaryPaths;
    private bool m_canProcess;

    #endregion

    #region Properties

    public ObservableCollection<BinaryPath> BinaryPaths
    {
      get => m_binaryPaths;
      set
      {
        m_binaryPaths = value;
        OnPropertyChanged(nameof(BinaryPaths));
      }
    }

    public string PathToOutput
    {
      get => m_pathToOutput;
      set
      {
        m_pathToOutput = value;
        OnPropertyChanged(nameof(PathToOutput));

        CanProcess = !string.IsNullOrEmpty(value);
      }
    }

    public bool CanProcess
    {
      get => m_canProcess;
      set
      {
        m_canProcess = value;
        OnPropertyChanged(nameof(CanProcess));
      }
    }

    #endregion

    #region Commands

    public ICommand BrowseOutputPathCommand
      => new RelayCommand(() =>
      {
        var dialog = new FolderBrowserDialog
        {
          ShowNewFolderButton = true,
          UseDescriptionForTitle = false
        };

        if (dialog.ShowDialog() != DialogResult.OK) return;
        PathToOutput = dialog.SelectedPath;
      });

    public ICommand LoadCommand
      => new RelayCommand(() => { });

    public ICommand SaveCommand
      => new RelayCommand(() => { });

    public ICommand SaveAsCommand
      => new RelayCommand(() => { });

    public ICommand ProcessCommand
      => new RelayCommand(async () =>
      {
        m_cancellation = new CancellationTokenSource();

        var missingDocs = BinaryPaths.Where(x => !x.DocumentationLinked).ToArray();
        if (missingDocs.Any())
        {
          var canContinue = MessageBox.Show($"There are binaries which do not have any XML documentation linked.{Environment.NewLine}{string.Concat(Environment.NewLine, missingDocs)}.{Environment.NewLine}Do you want to continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
          if (canContinue == DialogResult.No) return;
        }

        var result = await ProcessBinaries().ConfigureAwait(false);
        if (result)
          MessageBox.Show("Documentation generated successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        else
          MessageBox.Show("Operation cancelled.", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
      });

    public ICommand AddNewCommand
      => new RelayCommand(() =>
      {
        var openFileDialog = new OpenFileDialog
        {
          Multiselect = true,
          Filter = "Executable (*.exe)|*.exe|Library (*.dll)|*.dll",
          InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        };

        if (openFileDialog.ShowDialog() != DialogResult.OK) return;
        foreach (var filename in openFileDialog.FileNames)
        {
          var xmlDoc = filename.Substring(0, filename.LastIndexOf('.')) + ".xml";

          if (!File.Exists(xmlDoc))
            xmlDoc = string.Empty;

          BinaryPaths.Add(new BinaryPath { PathToBinary = filename, PathToDocumentation = xmlDoc });
        }
      });

    public ICommand RemoveCommand
      => new RelayCommand<BinaryPath>(x => BinaryPaths.Remove(x));

    #endregion

    public MainWindowViewModel()
      => BinaryPaths = new ObservableCollection<BinaryPath>();

    #region Methods

    private async Task<bool> ProcessBinaries()
      => await Task.Run(() =>
        {
          try
          {
            var pairs = BindAssembliesToDocs();

            var assemblyPages = pairs.Select(pair => new Page(pair.ItemA.FullName.Substring(0, pair.ItemA.FullName.IndexOf(",", StringComparison.InvariantCulture)))
                                     .SetContent(Generator.Generate(pair.ItemA, pair.ItemB)));
            var mainPage = new Page("Home");
            mainPage.SetContent(assemblyPages);

            Exporter.Export(mainPage, PathToOutput, m_cancellation.Token);

            return true;
          }
          catch (OperationCanceledException)
          {
            return false;
          }
        })
        .ConfigureAwait(false);

    private IEnumerable<Pair<Assembly, XmlDocument>> BindAssembliesToDocs()
    {
      foreach (var binaryPath in BinaryPaths)
      {
        m_cancellation.Token.ThrowIfCancellationRequested();

        if (!File.Exists(binaryPath.PathToBinary))
          continue;

        var documentation = new XmlDocument();
        try
        {
          documentation.Load(Path.Combine(
            Path.GetDirectoryName(binaryPath.PathToBinary) ?? throw new ArgumentException("Invalid path."),
            $"{binaryPath.BinaryFilename ?? throw new ArgumentException("Invalid path.")}.{Constants.EXT_XML}"));
        }
        catch (Exception)
        {
          continue;
        }

        try
        {
          documentation = new XmlDocument();
          documentation.Load(binaryPath.PathToDocumentation);
        }
        catch (Exception)
        {
          continue;
        }

        yield return new Pair<Assembly, XmlDocument>(Assembly.LoadFile(binaryPath.PathToBinary), documentation);
      }
    }

    #endregion
  }
}