﻿using System.Windows;
using MarkDoc.ViewModels;

namespace MarkDoc.Views
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  /// <inheritdoc cref="Window" />
  public partial class MainWindow
    : Window
  {
    /// <summary>
    /// ViewModel for the View
    /// </summary>
    public MainWindowViewModel ViewModel
    {
      get => (MainWindowViewModel) DataContext;
      set => DataContext = value;
    }

    /// <summary>
    /// View default constructor
    /// </summary>
    /// <inheritdoc />
    public MainWindow()
    {
      ViewModel = new MainWindowViewModel();

      InitializeComponent();
    }
  }
}