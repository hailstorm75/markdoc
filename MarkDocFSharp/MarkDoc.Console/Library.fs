namespace MarkDoc.Structure

open System
open System.Reflection
open Microsoft.FSharp.Collections

type Namespace =
| Node of string * option<Set<Namespace>>

[<Struct>]
type Library =
  val private m_assembly   : Assembly
  val private m_namespaces : Set<Namespace>

  member this.Assembly   with get() = this.m_assembly
  member this.FullName   with get() = this.m_assembly.FullName
  member this.Namespaces with get() = this.m_namespaces

  new (assembly : Assembly) =
    if isNull assembly then
      raise <| ArgumentException("assembly")

    let namespaces =
      let splitNamespaces (nsChain : String list option) =
        match nsChain.Value with
        | [hd]  -> (hd, None)
        | first::tail  -> (first, Some(tail))
        | _ -> raise <| Exception()

      let rec generateTree (namespaces : seq<String list option>) =
        let ifEmpty input =
          if Set.isEmpty input then None
          else Some(input)

        namespaces
        |> Seq.map splitNamespaces
        |> Seq.groupBy fst
        |> Seq.map (fun x -> Node(fst x, x
                                         |> snd
                                         |> Seq.map snd
                                         |> Seq.filter Option.isSome
                                         |> generateTree
                                         |> ifEmpty))
        |> Set.ofSeq

      assembly.GetTypes()
      |> Seq.map (fun x -> x.Namespace)
      |> Seq.filter (isNull >> not)
      |> Seq.distinct
      |> Seq.sort
      |> Seq.map (fun x -> x.Split('.') |> Array.toList |> Some)
      |> generateTree
      
    {
      m_assembly = assembly
      m_namespaces = namespaces
    }
