﻿namespace MarkDoc.Console

open System
open Commander.NET.Attributes

type Options() =
  /// <summary>
  /// Determines whether the program usage should be displayed
  /// </summary>
  [<Parameter("-h", "--help", Description = "Display console help", Required = Required.No)>]
  member val ShowHelp = false with get, set

  /// <summary>
  /// Paths to assemblies to process
  /// </summary>
  [<Parameter("-a", "--assemblies", Description = "Paths to assemblies to document", Required = Required.Yes)>]
  member val Assemblies = String.Empty with get, set

  /// <summary>
  /// Paths to assembly documentation
  /// </summary>
  [<Parameter("-d", "--documentation", Description = "Paths to documentation folder. NOTE: Use if the documentation is located in a different system location other than the same location as the paired assembly.", Required = Required.No)>]
  member val Documentation = String.Empty with get, set

  /// <summary>
  /// Paths to generated documentation output folder
  /// </summary>
  [<Parameter("-o", "--output", Description = "Paths to output folder", Required = Required.No)>]
  member val Output = String.Empty with get, set
