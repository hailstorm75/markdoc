﻿open Commander.NET
open Commander.NET.Exceptions
open MarkDoc.Console
open MarkDoc.Structure
open System.Reflection

/// <summary>
/// Parses arguments to an instance of <see cref="Options"/>
/// </summary>
/// <param name="arguments">Arguments to parse</param>
/// <returns>Instance of <see cref="Options"/></returns>
let parseArguments arguments =
  let parser = new CommanderParser<Options>()

  // Prints error message with usage instructions and returns None
  let printExExit message args =
    printfn message args
    printfn "%A" (parser.Usage())
    None

  parser.Add(arguments) |> ignore

  try
    Some(parser.Parse())
  with
  | :? ParameterMissingException as ex
    -> printExExit "Missing parameter: %s" ex.ParameterName
  | :? ParameterFormatException as ex
    -> printExExit "Invalid format used for parameter: %s" ex.ParameterName

let generateLibraries paths =
  let loadAssembly path =
    try
      Some(Assembly.LoadFile path)
    with
    | _ -> None

  paths
  |> Seq.map    loadAssembly
  |> Seq.filter Option.isSome
  |> Seq.map    (fun x -> Library x.Value)
  |> Seq.toList

[<EntryPoint>]
let main arguments =
  let run (options : Options) =
    let libraries = options.Assemblies.Split [|','|]
                    |> generateLibraries
    0

  match (parseArguments arguments) with
  | Some(opt) -> run opt
  | None    -> 1
